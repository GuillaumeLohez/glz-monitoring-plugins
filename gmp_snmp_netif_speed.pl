#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check speed of in an ethernet interface.
    It uses standard MIB-II.

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;
use Number::Format qw(format_bytes unformat_number);


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options             = ();
$options{'version'}     = 'master';
$options{'name'}        = basename("$0");
$options{'display'}     = 'Interface Speed';
$options{'details'}     = 'Check interface speed on a snmp server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}       = 0;
# Lifetime of cache file
$options{'lifetime'}    = 43200;


# Needed oids for this plugin
my %oids                = ();
$oids{'list_name'}      = '.1.3.6.1.2.1.31.1.1.1.1';
$oids{'list_desc'}      = '.1.3.6.1.2.1.2.2.1.2';
$oids{'list_status'}    = '.1.3.6.1.2.1.2.2.1.8';
$oids{'list_speed'}     = '.1.3.6.1.2.1.2.2.1.5';


# Init state
my $state   = 'UNKNOWN';
my %results = ();


# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
# Local parameters
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'interface',
    '',
    'i',
    'interface',
    '<interface>',
    'interface name',
    's'
);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'speed',
    '',
    'S',
    'speed',
    '<speed>',
    'speed expected',
    's'
);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'byname',
    undef,
    'n',
    'byname',
    'false',
    'Use names OID of interfaces in place of descriptions OID',
    ''
);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in Snmp parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'interface')) {

    print "\nMissing interface name: -i|--interface\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'speed')) {

    print "\nMissing interface speed: -S|--speed\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(defined($parameters{'byname'}{'value'}) and $parameters{'protocol'}{'value'} eq '1') {

    print "\nName OID not compatible with SNMP v1\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}


# Cache file
my $interface_name_clean = clean_name(\%options, $parameters{'interface'}{'value'});
my $cache_file = "/var/cache/glz/$options{'name'}-$parameters{'host'}{'value'}-if_$interface_name_clean.cache";


# Get id from cache
$results{'interface_id'} = read_cache_file(\%options, $cache_file);

if(not defined($results{'interface_id'})) {

    write_debug_if_requested(\%options, 'Cache file is too old or does not exist, snmpwalk needed');

    if(defined($parameters{"byname"}{'value'})) {

        write_debug_if_requested(\%options, 'use name OID to get list of interfaces');

        $results{'interface_id'} = found_snmp_interface_id(\%options, \%parameters, $oids{'list_name'}, $parameters{'interface'}{'value'});
    }
    else {

        write_debug_if_requested(\%options, 'use desc OID to get list of interfaces');

        $results{'interface_id'} = found_snmp_interface_id(\%options, \%parameters, $oids{'list_desc'}, $parameters{'interface'}{'value'});
    }

    if(not (test_snmp_value(\%options, $results{'interface_id'}))) {

        unlink($cache_file);
        write_plugin_output(\%options, $state, 'Can not get interface id');
        exit($ERRORS{"$state"});
    }
    else {

        # Update cache file
        my $res = update_cache_file(\%options, $cache_file, $results{'interface_id'});
        if(not $res) {

            write_plugin_output(\%options, $state, 'Can not write cache file');
            exit($ERRORS{"$state"});
        }
    }
}


# Create the status OID for this interface
$oids{'status'} = "$oids{'list_status'}.$results{'interface_id'}";
$oids{'speed'}  = "$oids{'list_speed'}.$results{'interface_id'}";


# if not UP, we can exit !
write_debug_if_requested(\%options, "get interface status with OID: $oids{'status'}");
if(not (is_interface_up(\%options, \%parameters, $oids{'status'}))) {

    write_plugin_output(\%options, $state, "Interface $parameters{'interface'}{'value'} is not up");
    exit($ERRORS{"$state"});
}


# Get current values by Snmp
SNMP:
foreach my $key ('speed') {

    write_debug_if_requested(\%options, "Snmp get key: $key");
    write_debug_if_requested(\%options, 'with OID: '.$oids{"$key"});

    $results{"$key"} = get_oid(\%options, \%parameters, $oids{"$key"});

    if(not (test_snmp_value(\%options, $results{"$key"}))
        or $results{"$key"} !~ /^\d+$/) {

        unlink($cache_file);
        write_plugin_output(\%options, $state, "Can not get interface $key (cache problem ? anyway cache deleted !)\n");
        exit($ERRORS{"$state"});
    }

    write_debug_if_requested(\%options, "result, $key: ".$results{"$key"});
}


# Differents cases of exit
my @withError = ();

if(unformat_number($parameters{'speed'}{'value'}, base => 1000) ne $results{'speed'}) {

    $state = 'CRITICAL';
    push(@withError, 'speed');
}

if(scalar(@withError) == '0') {

    $state = "OK";
}


# exit of plugin
my $msg = "On $parameters{'interface'}{'value'}: ";
my $expected = undef;

if($parameters{'speed'}{'value'} =~ /^\d+$/) {

    $expected = format_bytes($parameters{'speed'}{'value'}, base => 1000).'bits/s';
}
else {

    $expected = "$parameters{'speed'}{'value'}bits/s";
}

if($state eq "CRITICAL") {

    CRITICAL:
    foreach my $error (@withError) {

        $msg .= "$error is ".format_bytes($results{"$error"}, base => 1000)."bits/s but must be: $expected";
    }
}
else {

    $msg .= "speed is $expected"
}

write_plugin_output(\%options, $state, "$msg");

print "\n";
exit($ERRORS{"$state"});

