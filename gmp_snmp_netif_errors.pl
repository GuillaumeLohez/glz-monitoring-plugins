#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check error and discard in/out counters of an ethernet interface.
    It uses standard MIB-II.

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


# Standard includes
use strict;
use warnings;
use File::Basename;
use Number::Format qw(format_bytes unformat_number);


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options             = ();
$options{'version'}     = 'master';
$options{'name'}        = basename("$0");
$options{'display'}     = 'Interface errors';
$options{'details'}     = 'Check errors/discards packets of an interface on a server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}       = 0;
# Lifetime of cache file
$options{'lifetime'}    = 43200;


# Needed oids for this plugin
my %oids                    = ();
$oids{'list_name'}          = '.1.3.6.1.2.1.31.1.1.1.1';
$oids{'list_desc'}          = '.1.3.6.1.2.1.2.2.1.2';
$oids{'list_status'}        = '.1.3.6.1.2.1.2.2.1.8';
$oids{'list_in_discards'}   = '.1.3.6.1.2.1.2.2.1.13';
$oids{'list_out_discards'}  = '.1.3.6.1.2.1.2.2.1.19';
$oids{'list_in_errors'}     = '.1.3.6.1.2.1.2.2.1.14';
$oids{'list_out_errors'}    = '.1.3.6.1.2.1.2.2.1.20';


# Init state
my $state   = 'UNKNOWN';
my %results = ();


# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'interface',
    '',
    'i',
    'interface',
    '<interface>',
    'interface name',
    's'
);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'byname',
    undef,
    'n',
    'byname',
    'false',
    'Use names OID of interfaces in place of descriptions OID',
    ''
);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in Snmp parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_thresholds(\%options, \%parameters)) {

    print "\nMistake in warning/critical parameters: critical must be > warning\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'interface')) {

    print "\nMissing interface: -i|--interface\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(defined($parameters{'byname'}{'value'}) and $parameters{'protocol'}{'value'} eq '1') {
    print "\nName OID not compatible with SNMP v1\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}


# Cache & previous files
my $interface_name_clean = clean_name(\%options, $parameters{'interface'}{'value'});
my $cache_file      = "/var/cache/glz/$options{'name'}-$parameters{'host'}{'value'}-if_$interface_name_clean.cache";
my $previous_file   = "/var/cache/glz/$options{'name'}-$parameters{'host'}{'value'}-if_$interface_name_clean.previous";


# Get cached id
$results{'interface_id'} = read_cache_file(\%options, $cache_file);

if(not defined($results{'interface_id'})) {

    write_debug_if_requested(\%options, 'Cache file is too old or does not exist, snmpwalk needed');

    if(defined($parameters{"byname"}{'value'})) {

        write_debug_if_requested(\%options, 'use name OID to get list of interfaces');

        $results{'interface_id'} = found_snmp_interface_id(\%options, \%parameters, $oids{'list_name'}, $parameters{'interface'}{'value'});
    }
    else {

        write_debug_if_requested(\%options, 'use desc OID to get list of interfaces');

        $results{'interface_id'} = found_snmp_interface_id(\%options, \%parameters, $oids{'list_desc'}, $parameters{'interface'}{'value'});
    }

    if(not (test_snmp_value(\%options, $results{'interface_id'}))) {

        unlink($cache_file);
        write_plugin_output(\%options, $state, "Can not get interface id\n");
        exit($ERRORS{"$state"});
    }
    else {

        # Update cache file
        my $res = update_cache_file(\%options, $cache_file, $results{'interface_id'});

        if(not $res) {
            write_plugin_output(\%options, $state, "Can not write cache file\n");
            exit($ERRORS{"$state"});
        }
    }
}


# init some values
my $no_previous_value   = 0;
my $interface_reset     = 0;
$results{'diff_time'}           = undef;
$results{'diff_in_errors'}      = undef;
$results{'diff_out_errors'}     = undef;
$results{'diff_in_discards'}    = undef;
$results{'diff_out_discards'}   = undef;


# Create custom OIDs for this interface
$oids{'status'}         = "$oids{'list_status'}.$results{'interface_id'}";
$oids{'in_errors'}      = "$oids{'list_in_errors'}.$results{'interface_id'}";
$oids{'out_errors'}     = "$oids{'list_out_errors'}.$results{'interface_id'}";
$oids{'in_discards'}    = "$oids{'list_in_discards'}.$results{'interface_id'}";
$oids{'out_discards'}   = "$oids{'list_out_discards'}.$results{'interface_id'}";


# if not UP, we can exit !
write_debug_if_requested(\%options, "get interface status with OID: $oids{'status'}");
if(not (is_interface_up(\%options, \%parameters, $oids{'status'}))) {

    write_plugin_output(\%options, $state, "Interface $parameters{'interface'}{'value'} is not up\n");

    exit($ERRORS{"$state"});
}


# Get time 
$results{'time'} = time();
write_debug_if_requested(\%options, "time: $results{'time'}");


# Init of previous values
$results{'previous_time'}           = undef;
$results{'previous_in_errors'}      = undef;
$results{'previous_out_errors'}     = undef;
$results{'previous_in_discards'}    = undef;
$results{'previous_out_discards'}   = undef;


# Get previous value
my $readRes = read_previous_file(\%options, $previous_file, \%results);
if(not $readRes) {

    $no_previous_value = 1;
}


# Get current values by Snmp
SNMP:
foreach my $key ('in_errors', 'out_errors', 'in_discards', 'out_discards') {

    write_debug_if_requested(\%options, "Snmp get key: $key");
    write_debug_if_requested(\%options, 'with OID: '.$oids{"$key"});


    $results{"$key"} = get_oid(\%options, \%parameters, $oids{"$key"});
    if(not (test_snmp_value(\%options, $results{"$key"}))
        or $results{"$key"} !~ /^\d+$/) {

        unlink($cache_file);
        write_plugin_output(\%options, $state, "Can not get interface: $key (cache problem ? anyway, deleted !)\n");
        exit($ERRORS{"$state"});
    }

    write_debug_if_requested(\%options, "result, $key: ".$results{"$key"});
}


# debug
if($options{'debug'}) {

    foreach my $key ('time', 'in_errors', 'out_errors', 'in_discards', 'out_discards') {

        my $previous_key = "previous_$key";

        if(defined($results{"$previous_key"})) {

            write_debug_if_requested(\%options, "prev, $key: ".$results{"$previous_key"});
        }
        else {

            write_debug_if_requested(\%options, "prev, $key: not found");
        }
    }
}


# Test if all values are defined or not. 
# If not, write file and exit with unknown status
if(
    not defined($results{'previous_time'})
    or not defined($results{'previous_in_errors'})
    or not defined($results{'previous_out_errors'})
    or not defined($results{'previous_in_discards'})
    or not defined($results{'previous_out_discards'})
) {

    write_debug_if_requested(\%options, 'no previous value');
    $no_previous_value = 1;
}
else {

    # start calculating all values
    # If previous is higher then new, interface has been reseted
    write_debug_if_requested(\%options, 'calculate error load');

    $results{'diff_time'} = $results{'time'} - $results{'previous_time'};

    CALCULATE:
    foreach my $key ('in_errors', 'out_errors', 'in_discards', 'out_discards') {

        my $previous_key = "previous_$key";
        my $diff_key = "diff_$key";

        if($results{"$previous_key"} > $results{"$key"}) {

            $interface_reset = 1;
            write_debug_if_requested(\%options, 'interface reset');
        }
        else {

            $results{"$diff_key"} = $results{"$key"} - $results{"$previous_key"};
            write_debug_if_requested(\%options, "diff $key: ".$results{"$diff_key"});
        }
    }
}


# Write previous file
my $writeRes = update_previous_file(\%options, $previous_file, \%results);
if(not $writeRes) {

    write_plugin_output(\%options, $state, "can not write previous file\n");
	exit($ERRORS{"$state"});
}


# If interface reset detected, exit unknown
if($interface_reset) {

    write_plugin_output(\%options, $state, "$parameters{'interface'}{'value'} has been reseted on this server\n");
	exit($ERRORS{"$state"});
}


# If not previous file, exit unknown
if($no_previous_value) {

    write_plugin_output(\%options, $state, "No previous file for interface: $parameters{'interface'}{'value'}. Wait for next check\n");
	exit($ERRORS{"$state"});
}


# Differents cases of exit
my @withError = ();

if(defined($parameters{'critical'}{'value'})) {

    CRITICAL:
    foreach my $key ('in_errors', 'out_errors', 'in_discards', 'out_discards') {

        my $diff_key = "diff_$key";

        if($results{"$diff_key"} > unformat_number($parameters{'critical'}{'value'}, base => 1000)) {

            $state = 'CRITICAL';
            push(@withError, "$key");
        }
    }
}

if(defined($parameters{'warning'}{'value'}) and $state ne "CRITICAL") {

    WARNING:
    foreach my $key ('in_errors', 'out_errors', 'in_discards', 'out_discards') {

        my $diff_key = "diff_$key";

        if($results{"$diff_key"} > unformat_number($parameters{'warning'}{'value'}, base => 1000)) {
            $state = 'WARNING';
            push(@withError, "$key");
        }
    }
}

if(scalar(@withError) == '0') {

    $state = "OK";
}

# convert to human readable
$results{'display_in_errors'}       = format_bytes($results{'diff_in_errors'}, base => 1000).'pckt(s)';
$results{'display_out_errrors'}     = format_bytes($results{'diff_out_errors'}, base => 1000).'pckt(s)';
$results{'display_in_discards'}     = format_bytes($results{'diff_in_errors'}, base => 1000).'pckt(s)';
$results{'display_out_discards'}    = format_bytes($results{'diff_out_errors'}, base => 1000).'pckt(s)';


# exit of plugin
my $msg = "On $parameters{'interface'}{'value'}: ";
if($state eq "WARNING" or $state eq "CRITICAL") {

    ERRORS:
    foreach my $error (@withError) {

        my $diffError = "diff_$error";
        $msg .= "$error: ".$results{"$diffError"};
    }
}
else {

    $msg .= 'Error/Discard packets under thresholds';
}

$msg .= " in the last $results{'diff_time'} seconds";

write_plugin_output(\%options, $state, "$msg");
if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();
    PERFDATA:
    foreach my $key ('in_errors', 'out_errors', 'in_discards', 'out_discards') {

        my $diff_key = "diff_$key";
        $perfData{"$key"}{'value'} = $results{"$diff_key"};

        if(defined($parameters{'warning'}{'value'})) {
            $perfData{"$key"}{'warning'} = unformat_number($parameters{'warning'}{'value'}, base => 1000);
        }

        if(defined($parameters{'critical'}{'value'})) {
            $perfData{"$key"}{'critical'} = unformat_number($parameters{'critical'}{'value'}, base => 1000);
        }
    }

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

