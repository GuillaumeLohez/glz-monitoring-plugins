#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check used disk space on a server.
    It uses standard MIB-II.

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options             = ();
$options{'version'}     = 'master';
$options{'name'}        = basename("$0");
$options{'display'}     = 'Disk usage';
$options{'details'}     = 'Check particular disk usage on a snmp server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}       = 0;
# Lifetime of cache file
$options{'lifetime'}    = 43200;


# Needed oids for this plugin
my %oids            = ();
$oids{'list_disk'}  = '.1.3.6.1.2.1.25.2.3.1.3';
$oids{'list_size'}  = '.1.3.6.1.2.1.25.2.3.1.5';
$oids{'list_used'}  = '.1.3.6.1.2.1.25.2.3.1.6';


# Init state
my $state   = 'UNKNOWN';
my %results = ();



# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
# Local parameters
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'disk',
    '',
    'd',
    'disk',
    '<partition>',
    'partition name',
    's'
);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in Snmp parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_thresholds(\%options, \%parameters)) {

    print "\nMistake in warning/critical parameters: critical must be > warning\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'disk')) {

    print "\nMissing disk mount point: -d|--disk\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}


# Cache file
my $disk_name_clean = clean_name(\%options, $parameters{'disk'}{'value'});
my $cache_file = "/var/cache/glz/$options{'name'}-$parameters{'host'}{'value'}-disk_$disk_name_clean.cache";


# Get cached id
$results{'disk_id'} = read_cache_file(\%options, $cache_file);

if(not defined($results{'disk_id'})) {

    write_debug_if_requested(\%options, 'Cache file is too old or does not exist, snmpwalk needed');

    # Get the id of the requested partition
    $results{'disk_id'} = found_snmp_disk_id(\%options, \%parameters, $oids{'list_disk'}, $parameters{'disk'}{'value'});

    if(not (test_snmp_value(\%options, $results{'disk_id'}))) {

	    unlink($cache_file);
        write_plugin_output(\%options, $state, "Can not get disk id\n");
        exit($ERRORS{"$state"});
    }
    else {

        # Update cache file
        my $res = update_cache_file(\%options, $cache_file, $results{'disk_id'});

        if(not $res) {
            write_plugin_output(\%options, $state, "Can not write cache file\n");
            exit($ERRORS{"$state"});
        }
    }
}


# Create specific OID
$oids{'used'} = "$oids{'list_used'}.$results{'disk_id'}";
$oids{'size'} = "$oids{'list_size'}.$results{'disk_id'}";


# Get value
SNMP:
foreach my $key ('size', 'used') {

    write_debug_if_requested(\%options, "Snmp get key: $key");
    write_debug_if_requested(\%options, 'with OID: '.$oids{"$key"});

    $results{"$key"} = get_oid(\%options, \%parameters, $oids{"$key"});

    if(not (test_snmp_value(\%options, $results{"$key"}))
        or $results{"$key"} !~ /^\d+$/) {

        unlink($cache_file,);
        write_plugin_output(\%options, $state, "Can not get disk $key (cache problem ? anyway, deleted !)\n");
        exit($ERRORS{"$state"});
    }

    write_debug_if_requested(\%options, "result, $key: ".$results{"$key"});
}


# Calculate percent of used space
$results{'percent'} = calculate_percent(\%options, $results{'used'}, $results{'size'});

if(not defined($results{'percent'})) {

    write_plugin_output(\%options, $state, "Can not calculate percent of used disk\n");
    exit($ERRORS{"$state"});
}
write_debug_if_requested(\%options, "percent used disk: $results{'percent'}");

# Different cases of exit
if(defined($parameters{'critical'}{'value'}) and ($results{'percent'} > $parameters{'critical'}{'value'})) {

    $state = 'CRITICAL';
}
elsif(defined($parameters{'warning'}{'value'}) and ($results{'percent'} > $parameters{'warning'}{'value'})) {

    $state = 'WARNING';
}
else {

    $state = 'OK';
}


# exit of plugin
write_plugin_output(\%options, $state, "$results{'percent'}% disk used for $parameters{'disk'}{'value'}");
if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();
    $perfData{'used_disk_'.$parameters{'disk'}{'value'}}{'value'} = $results{'percent'};

    if(defined($parameters{'warning'}{'value'})) {

        $perfData{'used_disk_'.$parameters{'disk'}{'value'}}{'warning'} = "$parameters{'warning'}{'value'}";
    }

    if(defined($parameters{'critical'}{'value'})) {

        $perfData{'used_disk_'.$parameters{'disk'}{'value'}}{'critical'} = "$parameters{'critical'}{'value'}";
    }

    $perfData{'used_disk_'.$parameters{'disk'}{'value'}}{'min'} = '0';
    $perfData{'used_disk_'.$parameters{'disk'}{'value'}}{'max'} = '100';

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

