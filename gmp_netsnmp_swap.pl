#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check swap usage of a linux server.
    It uses Net-SNMP MIB.

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = basename("$0");
$options{'display'} = 'Swap usage';
$options{'details'} = 'Check swap usage on a linux server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}   = 0;


# Needed oids for this plugin
my %oids        = ();
$oids{'free'}   = '1.3.6.1.4.1.2021.4.4.0';
$oids{'total'}  = '1.3.6.1.4.1.2021.4.3.0';


# Init state
my $state   = 'UNKNOWN';
my %results = ();


# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in Snmp parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_thresholds(\%options, \%parameters)) {

    print "\nMistake in warning/critical parameters: critical must be > warning\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}


# Work on snmp
SNMP:
foreach my $key ('free', 'total') {

    write_debug_if_requested(\%options, "Snmp get key: $key");
    write_debug_if_requested(\%options, 'with OID: '.$oids{"$key"});

    $results{"$key"} = get_oid(\%options, \%parameters, $oids{"$key"});

    if(not (test_snmp_value(\%options, $results{"$key"}))) {

        write_plugin_output(\%options, $state, "Can not get $key swap\n");

        exit($ERRORS{"$state"});
    }

    write_debug_if_requested(\%options, "result, $key: ".$results{"$key"});
}


# calculate used swap
$results{used} = $results{'total'}-$results{'free'};
write_debug_if_requested(\%options, "used swap: $results{'used'}");


# Calculate percent of used space
$results{'percent'} = calculate_percent(\%options, $results{used}, $results{'total'});
if(not defined($results{'percent'})) {

    write_plugin_output(\%options, $state, "Can not calculate percent of used swap\n");

    exit($ERRORS{"$state"});
}
write_debug_if_requested(\%options, "percent used swap: $results{'percent'}");



# Different cases of exit
if(defined($parameters{'critical'}{'value'}) and ($results{'percent'} > $parameters{'critical'}{'value'})) {

    $state = 'CRITICAL';
}
elsif(defined($parameters{'warning'}{'value'}) and ($results{'percent'} > $parameters{'warning'}{'value'})) {

    $state = 'WARNING';
}
else {

    $state = 'OK';
}


# exit of plugin
write_plugin_output(\%options, $state, "$results{'percent'}% swap used");
if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();
    $perfData{'used_swap'}{'value'} = $results{'percent'};

    if(defined($parameters{'warning'}{'value'})) {

        $perfData{'used_swap'}{'warning'} = "$parameters{'warning'}{'value'}";
    }
    if(defined($parameters{'critical'}{'value'})) {

        $perfData{'used_swap'}{'critical'} = "$parameters{'critical'}{'value'}";
    }

    $perfData{'used_swap'}{'min'} = '0';
    $perfData{'used_swap'}{'max'} = '100';

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

