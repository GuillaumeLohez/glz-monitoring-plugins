#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check CPU(s) usage of a linux server.
    It uses Net-SNMP MIB.

Creation:
    April 2015

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = basename("$0");
$options{'display'} = 'CPU Load';
$options{'details'} = 'check CPU(s) usage of a linux server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}   = 0;


# Needed oids for this plugin
my %oids        = ();
$oids{'usage'}  = '.1.3.6.1.2.1.25.3.3.1.2';


# Init state
my $state   = 'UNKNOWN';
my %results = ();


# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
%parameters = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in SNMP parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(defined($parameters{'warning'}{'value'}) and not check_parameter_percent(\%options, \%parameters, 'warning')) {

    print "\nWarning thresholds (-w|--warning) must be percent\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}
if(defined ($parameters{'critical'}{'value'}) and not check_parameter_percent(\%options, \%parameters, 'critical')) {

    print "\nCritical thresholds (-c|--critical) must be percent\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}


# Work on snmp
my %processors = ();

SNMP:
foreach my $key ('usage') {

    write_debug_if_requested(\%options, "Snmp get key: $key");
    write_debug_if_requested(\%options, 'with OID: '.$oids{"$key"});

    %processors = walk_oid(\%options, \%parameters, $oids{'usage'});
}

if(not (test_snmp_values(\%options, \%processors))) {

    write_plugin_output(\%options, $state, "Can not get cpu usage(s)\n");
    exit($ERRORS{"$state"});
}


# Check every processor(s)
my $usage = undef;
my $nb = 0;

AVERAGE:
foreach my $value (values(%processors)) {

    $nb++;
    $usage += $value;
}
$usage = int($usage / $nb);


# Different cases of exit
if(defined($parameters{'critical'}{'value'}) and $usage > $parameters{'critical'}{'value'}) {

    $state = 'CRITICAL';
}
elsif(defined($parameters{'warning'}{'value'}) and $usage > $parameters{'warning'}{'value'}) {

    $state = 'WARNING';
}
else {

    $state = 'OK';
}


# exit of plugin
my $msg = "Usage: $usage\% over $nb CPU(s)";
write_plugin_output(\%options, $state, "$msg");

if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();
    $perfData{'cpu_usage'}{'value'} = "$usage";

    if(defined($parameters{'warning'}{'value'})) {

        $perfData{'cpu_usage'}{'warning'} = "$parameters{'warning'}{'value'}";
    }

    if(defined($parameters{'critical'}{'value'})) {

        $perfData{'cpu_usage'}{'critical'} = "$parameters{'critical'}{'value'}";
    }

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

