#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check all used disk space on a server.
    It uses standard MIB-II.

Creation:
    April 2015

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options             = ();
$options{'version'}     = 'master';
$options{'name'}        = basename("$0");
$options{'display'}     = 'All disks usage';
$options{'details'}     = 'Check all disks usage on a snmp server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}       = 0;
# Lifetime of cache file
$options{'lifetime'}    = 43200;


# Needed oids for this plugin
my %oids            = ();
$oids{'list_disk'}  = '.1.3.6.1.2.1.25.2.3.1.3';
$oids{'list_size'}  = '.1.3.6.1.2.1.25.2.3.1.5';
$oids{'list_used'}  = '.1.3.6.1.2.1.25.2.3.1.6';


# Init state
my $state   = 'UNKNOWN';
my %results = ();



# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
# Local parameters
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'exclude',
    '',
    'e',
    'exclude',
    '<partition>[,<partition>,...]',
    'partition name to exclude',
    's'
);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in Snmp parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_thresholds(\%options, \%parameters)) {

    print "\nMistake in warning/critical parameters: critical must be > warning\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

# Cache file
my $cache_file = "/var/cache/glz/$options{'name'}-$parameters{'host'}{'value'}.cache";

# Get cached id
my $list = read_cache_file(\%options, $cache_file);
my %partitions = ();

if(not defined($list)) {

    write_debug_if_requested(\%options, 'Cache file is too old or does not exist, snmpwalk needed');

    # Get partitions list
    %partitions = build_snmp_disk_list(\%options, \%parameters, $oids{'list_disk'});

    if(not (test_snmp_values(\%options, \%partitions))) {

        unlink($cache_file);
        write_plugin_output(\%options, $state, "Can not get disk list\n");
        exit($ERRORS{"$state"});
    }
    else {

        # Update cache file
        while(my($name, $id) = each(%partitions)) {
            $list .= "$name,$id;";
        }
        my $res = update_cache_file(\%options, $cache_file, $list);

        if(not $res) {
            write_plugin_output(\%options, $state, "Can not write cache file\n");
            exit($ERRORS{"$state"});
        }
    }
}
else {

    my @disks = split(/;/, $list);
    foreach my $disk (@disks) {

        my @value = split(/,/, $disk);
        if($value[0]) {
            $partitions{$value[0]} = $value[1];
        }
    }
}


# Work on each partitions
my @withCrit    = ();
my @withWarn    = ();
my @withOk      = ();


PARTITIONS:
while(my($name, $id) = each(%partitions)) {

    write_debug_if_requested(\%options, "Disk: $name - Id: $id");

    # Create specific OID
    $oids{$name."_used"} = $oids{'list_used'}.'.'.$id;
    $oids{$name."_size"} = $oids{'list_size'}.'.'.$id;


    # Get values
    SNMP:
    foreach my $key ('size', 'used') {

        write_debug_if_requested(\%options, "Snmp get key: $key");
        write_debug_if_requested(\%options, 'with OID: '.$oids{$name.'_'.$key});

        $results{$name.'_'.$key} = get_oid(\%options, \%parameters, $oids{$name.'_'.$key});

        if(not (test_snmp_value(\%options, $results{$name.'_'.$key}))
            or $results{$name.'_'.$key} !~ /^\d+$/) {

            unlink($cache_file);
            write_plugin_output(\%options, $state, "Can not get disk $name : $key (cache problem ? anyway deleted !)\n");
            exit($ERRORS{"$state"});
        }

        write_debug_if_requested(\%options, "result, $name - $key : ".$results{$name.'_'.$key});
    }


    # Calculate percent of used space
    $results{$name.'_percent'} = calculate_percent(\%options, $results{$name.'_used'}, $results{$name.'_size'});

    if(not defined($results{$name.'_percent'})) {

        write_plugin_output(\%options, $state, "Can not calculate percent of used disk: $name\n");
        exit($ERRORS{"$state"});
    }
    write_debug_if_requested(\%options, "percent used disk $name: $results{$name.'_percent'}\%");


    # Different cases of exit
    if(defined($parameters{'critical'}{'value'}) and ($results{$name.'_percent'} > $parameters{'critical'}{'value'})) {

        write_debug_if_requested(\%options, "Push $name in Critical list");
        push(@withCrit, $name);
    }
    elsif(defined($parameters{'warning'}{'value'}) and ($results{$name.'_percent'} > $parameters{'warning'}{'value'})) {

        write_debug_if_requested(\%options, "Push $name in Warning list");
        push(@withWarn, $name);
    }
    else {

        write_debug_if_requested(\%options, "Push $name in Ok list");
        push(@withOk, $name);
    }
}

# exit of plugin
my $msg = '';
if(scalar(@withCrit) == '0' and scalar(@withWarn) == '0' and scalar(@withOk) != '0') {

    $state = 'OK';
    foreach my $part (@withOk) {

        $msg .= "[$part : $results{$part.'_percent'}\%] ";
    }
}
elsif(scalar(@withCrit) > '0') {

    $state = 'CRITICAL';
    foreach my $part (@withCrit) {

        $msg .= "[$part : $results{$part.'_percent'}\%] ";
    }
}
elsif(scalar(@withWarn) > '0') {

    $state = 'WARNING';
    foreach my $part (@withWarn) {

        $msg .= "[$part : $results{$part.'_percent'}\%] ";
    }
}


write_plugin_output(\%options, $state, "$msg");
if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();

    foreach my $part (@withOk, @withCrit, @withWarn) {

        $perfData{'used_disk_'.$part}{'value'} = $results{$part.'_percent'};

        if(defined($parameters{'warning'}{'value'})) {

            $perfData{'used_disk_'.$part}{'warning'} = "$parameters{'warning'}{'value'}";
        }

        if(defined($parameters{'critical'}{'value'})) {

            $perfData{'used_disk_'.$part}{'critical'} = "$parameters{'critical'}{'value'}";
        }

        $perfData{'used_disk_'.$part}{'min'} = '0';
        $perfData{'used_disk_'.$part}{'max'} = '100';
    }

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

