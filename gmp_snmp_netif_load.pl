#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check in/out bytes counters of an ethernet interface.
    It uses standard MIB-II.

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;
use Number::Format qw(format_bytes unformat_number);


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options             = ();
$options{'version'}     = 'master';
$options{'name'}        = basename("$0");
$options{'display'}     = 'Interface load';
$options{'details'}     = 'Check load of an interface on a snmp server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}       = 0;
# Lifetime of cache file
$options{'lifetime'}    = 43200;


# Needed oids for this plugin
my %oids                    = ();
$oids{'list_name'}          = '.1.3.6.1.2.1.31.1.1.1.1';
$oids{'list_desc'}          = '.1.3.6.1.2.1.2.2.1.2';
$oids{'list_status'}        = '.1.3.6.1.2.1.2.2.1.8';
$oids{'list_in_octets'}     = '.1.3.6.1.2.1.2.2.1.10';
$oids{'list_out_octets'}    = '.1.3.6.1.2.1.2.2.1.16';
$oids{'list_in_octets64'}   = '.1.3.6.1.2.1.31.1.1.1.6';
$oids{'list_out_octets64'}  = '.1.3.6.1.2.1.31.1.1.1.10';


# Init state
my $state   = 'UNKNOWN';
my %results = ();


# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'interface',
    '',
    'i',
    'interface',
    '<interface>',
    'interface name',
    's'
);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    '64bits',
    undef,
    'b',
    '64bits',
    'false',
    'Use of 64bits Snmp counters',
    ''
);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'byname',
    undef,
    'n',
    'byname',
    'false',
    'Use names OID of interfaces in place of descriptions OID',
    ''
);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in Snmp parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'interface')) {

    print "\nMissing interface: -i|--interface\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(defined($parameters{'byname'}{'value'}) and $parameters{'protocol'}{'value'} eq '1') {
    print "\nName OID not compatible with SNMP v1\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(defined($parameters{'64bits'}{'value'}) and $parameters{'protocol'}{'value'} eq '1') {

    print "\n64bits counters not compatible with SNMP v1\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

# Special job for different threshold in/out
my $warningIn   = undef;
my $warningOut  = undef;

if(defined($parameters{'warning'}{'value'}) and $parameters{'warning'}{'value'} =~ /,/) {

    write_debug_if_requested(\%options, 'in/out different thresholds on warning');

    my @warnings=split(/,/, $parameters{'warning'}{'value'});

    if(scalar(@warnings) != 2) {

        print "\nWarning threshold(s) must be <value><unit> or <value_in><unit>,<value_out><unit>\n\n";
        print_usage(\%options, \%parameters);
        exit($ERRORS{"$state"});
    }

    $warningIn  = unformat_number($warnings[0], base => 1024);
    $warningOut = unformat_number($warnings[1], base => 1024);
}
else {

    write_debug_if_requested(\%options, 'in/out same thresholds on warning');

    $warningIn  = unformat_number($parameters{'warning'}{'value'}, base => 1024);
    $warningOut = unformat_number($parameters{'warning'}{'value'}, base => 1024);
}

my $criticalIn  = undef;
my $criticalOut = undef;

if(defined($parameters{'critical'}{'value'}) and $parameters{'critical'}{'value'} =~ /,/) {

    write_debug_if_requested(\%options, 'in/out different thresholds on critical');

    my @criticals=split(/,/, $parameters{'critical'}{'value'});

    if(scalar(@criticals) != 2) {

        print "\nCritical threshold(s) must be <value><unit> or <value_in><unit>,<value_out><unit>\n\n";
        print_usage(\%options, \%parameters);
        exit($ERRORS{"$state"});
    }

    $criticalIn     = unformat_number($criticals[0], base => 1024);
    $criticalOut    = unformat_number($criticals[1], base => 1024);
}
else {

    write_debug_if_requested(\%options, 'in/out same thresholds on critical');

    $criticalIn     = unformat_number($parameters{'critical'}{'value'}, base => 1024);
    $criticalOut    = unformat_number($parameters{'critical'}{'value'}, base => 1024);
}

if(defined($criticalIn) and defined($warningIn) and $criticalIn < $warningIn) {

    print "\nCritical In threshold must be higher then warning In threshold\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(defined($criticalOut) and defined($warningOut) and $criticalOut < $warningOut) {

    print "\nCritical Out threshold must be higher then warning Out threshold\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

# Cache & previous files
my $interface_name_clean = clean_name(\%options, $parameters{'interface'}{'value'});
my $cache_file      = "/var/cache/glz/$options{'name'}-$parameters{'host'}{'value'}-if_$interface_name_clean.cache";
my $previous_file   = "/var/cache/glz/$options{'name'}-$parameters{'host'}{'value'}-if_$interface_name_clean.previous";



# Get previous value
$results{'interface_id'} = read_cache_file(\%options, $cache_file);

if(not defined($results{'interface_id'})) {

    write_debug_if_requested(\%options, 'Cache file is too old or does not exist, snmpwalk needed');

    if(defined($parameters{"byname"}{'value'})) {

        write_debug_if_requested(\%options, 'use name OID to get list of interfaces');

        $results{'interface_id'} = found_snmp_interface_id(\%options, \%parameters, $oids{'list_name'}, $parameters{'interface'}{'value'});
    }
    else {

        write_debug_if_requested(\%options, 'use desc OID to get list of interfaces');

        $results{'interface_id'} = found_snmp_interface_id(\%options, \%parameters, $oids{'list_desc'}, $parameters{'interface'}{'value'});
    }

    if(not (test_snmp_value(\%options, $results{'interface_id'}))) {

        unlink($cache_file);
        write_plugin_output(\%options, $state, "Can not get interface id\n");
        exit($ERRORS{"$state"});
    }
    else {

        # Update cache file
        my $res = update_cache_file(\%options, $cache_file, $results{'interface_id'});

        if(not $res) {

            write_plugin_output(\%options, $state, "Can not write cache file\n");
            exit($ERRORS{"$state"});
        }
    }
}


# init some values
my $no_previous_value   = 0;
my $interface_reset     = 0;
$results{'diff_time'}       = undef;
$results{'diff_in_octets'}  = undef;
$results{'diff_out_octets'} = undef;
$results{'in_load'}         = undef;
$results{'out_load'}        = undef;


# Create the status OID for this interface
$oids{'status'} = "$oids{'list_status'}.$results{'interface_id'}";

if(defined($parameters{"64bits"}{'value'})) {

    write_debug_if_requested(\%options, 'using 64bits counters');

    $oids{'in_octets'}   = "$oids{'list_in_octets64'}.$results{'interface_id'}";
    $oids{'out_octets'}  = "$oids{'list_out_octets64'}.$results{'interface_id'}";
}
else {

    write_debug_if_requested(\%options, 'using 32bits counters');

    $oids{'in_octets'}   = "$oids{'list_in_octets'}.$results{'interface_id'}";
    $oids{'out_octets'}  = "$oids{'list_out_octets'}.$results{'interface_id'}";
}


# if not UP, we can exit !
write_debug_if_requested(\%options, "get interface status with OID: $oids{'status'}");

if(not (is_interface_up(\%options, \%parameters, $oids{'status'}))) {

    write_plugin_output(\%options, $state, "Interface $parameters{'interface'}{'value'} is not up\n");
    exit($ERRORS{"$state"});
}


# Get time 
$results{'time'} = time();
write_debug_if_requested(\%options, "time: $results{'time'}");


# Init of previous values.
$results{'previous_time'}       = undef;
$results{'previous_in_octets'}  = undef;
$results{'previous_out_octets'} = undef;


# Get previous value
my $readRes = read_previous_file(\%options, $previous_file, \%results);
if(not $readRes) {

    $no_previous_value = 1;
}


# Get current values by Snmp
SNMP:
foreach my $key ('in_octets', 'out_octets') {

    write_debug_if_requested(\%options, "Snmp get key: $key");
    write_debug_if_requested(\%options, 'with OID: '.$oids{"$key"});

    $results{"$key"} = get_oid(\%options, \%parameters, $oids{"$key"});

    if(not (test_snmp_value(\%options, $results{"$key"}))
        or $results{"$key"} !~ /^\d+$/) {

        unlink($cache_file);
        write_plugin_output(\%options, $state, "Can not get interface $key (cache problem ? anyway cache deleted !)\n");
        exit($ERRORS{"$state"});
    }

    write_debug_if_requested(\%options, "result, $key: ".$results{"$key"});
}


# debug
if($options{'debug'}) {

    DEBUG:
    foreach my $key ('time', 'in_octets', 'out_octets') {

        my $previous_key = "previous_$key";

        if(defined($results{"$previous_key"})) {

            write_debug_if_requested(\%options, "prev, $key: ".$results{"$previous_key"});
        }
        else {

            write_debug_if_requested(\%options, "prev, $key: not found");
        }
    }
}



# Test if all values are defined or not. 
# If not, write file and exit with unknown status
if( not defined($results{'previous_time'})
    or not defined($results{'previous_in_octets'})
    or not defined($results{'previous_out_octets'})
) {

    write_debug_if_requested(\%options, 'no previous value');
    $no_previous_value = 1;
}
else {

    # start calculating all values
    write_debug_if_requested(\%options, 'calculate load');
    
    $results{'diff_time'} = $results{'time'} - $results{'previous_time'};

    CALCULATION:
    foreach my $key ('in_octets', 'out_octets') {

        my $previous_key = "previous_$key";
        my $diff_key = "diff_$key";

        if($results{"$previous_key"} > $results{"$key"} and not defined($parameters{'64bits'}{'value'})) {

            write_debug_if_requested(\%options, 'interface reset');

            $interface_reset = 1;
        }
        else {

            $results{"$diff_key"} = $results{"$key"} - $results{"$previous_key"};

            write_debug_if_requested(\%options, "diff $key: ".$results{"$diff_key"});
        }
    }

    $results{'in_load'} = int($results{'diff_in_octets'} * 8 / $results{'diff_time'});
    write_debug_if_requested(\%options, "load in: $results{'in_load'}");

    $results{'out_load'} = int($results{'diff_out_octets'} * 8 / $results{'diff_time'});
    write_debug_if_requested(\%options, "load out: $results{'out_load'}");
}


# Write previous file
my $writeRes = update_previous_file(\%options, $previous_file, \%results);
if(not $writeRes) {

    write_plugin_output(\%options, $state, "can not write previous file\n");
    exit($ERRORS{"$state"});
}


# If interface reset detected, exit unknown
if($interface_reset) {

    write_plugin_output(\%options, $state, "$parameters{'interface'}{'value'} has been reseted on this server (counter owerflow ?)\n");
    exit($ERRORS{"$state"});
}


# If not previous file, exit unknown
if($no_previous_value) {

    write_plugin_output(\%options, $state, "No previous file for interface: $parameters{'interface'}{'value'}. Wait for next check\n");
    exit($ERRORS{"$state"});
}


# Differents cases of exit
my @withError = ();

if(defined($criticalIn) and $results{'in_load'} > $criticalIn) {

    $state = 'CRITICAL';
    push(@withError, 'in_load');
}

if(defined($criticalOut) and $results{'out_load'} > $criticalOut) {

    $state = 'CRITICAL';
    push(@withError, 'out_load');
}

if(defined($warningIn) and $results{'in_load'} > $warningIn and $state ne 'CRITICAL') {

    $state = 'WARNING';
    push(@withError, 'in_load');
}

if(defined($warningOut) and $results{'out_load'} > $warningOut and $state ne 'CRITICAL') {

    $state = 'WARNING';
    push(@withError, 'out_load');
}

if(scalar(@withError) == '0') {

    $state = "OK";
}


# convert to human readable
$results{'display_in_load'}   = format_bytes($results{'in_load'}).'bits/s';
$results{'display_out_load'}  = format_bytes($results{'out_load'}).'bits/s';


# exit of plugin
my $msg = "On $parameters{'interface'}{'value'}: ";
if($state eq "WARNING" or $state eq "CRITICAL") {

    ERRORS:
    foreach my $error (@withError) {

        my $displayError = "display_$error";
        $msg .= " $error: ".$results{"$displayError"};
    }
}
else {

    $msg .= "load in: $results{'display_in_load'}, load out: $results{'display_out_load'}";
}

$msg .= " in the last $results{'diff_time'} seconds\n";

write_plugin_output(\%options, $state, "$msg");

if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();

    # In
    $perfData{'in_load'}{'value'} = $results{'in_load'};
    if(defined($warningIn)){

        $perfData{'in_load'}{'warning'} = $warningIn;
    }
    if(defined($criticalIn)){

        $perfData{'in_load'}{'critical'} = $criticalIn;
    }

    # Out
    $perfData{'out_load'}{'value'} = $results{'out_load'};
    if(defined($warningOut)){

        $perfData{'out_load'}{'warning'} = $warningOut;
    }
    if(defined($criticalOut)){

        $perfData{'out_load'}{'critical'} = $criticalOut;
    }

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

