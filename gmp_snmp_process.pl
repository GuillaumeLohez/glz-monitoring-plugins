#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check if process is present
    and eventually with a minimum number of process.
    It uses standard MIB-II.

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = basename("$0");
$options{'display'} = 'Process number';
$options{'details'} = 'Check number of a particular process with minimum possible on a snmp server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}   = 0;


# Needed oids for this plugin
my %oids                = ();
$oids{'list_process'}   = '.1.3.6.1.2.1.25.4.2.1.2';
$oids{'list_args'}      = '.1.3.6.1.2.1.25.4.2.1.5';


# Init state
my $state   = 'UNKNOWN';
my %results = ();


# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
# Local parameters
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'process',
    '',
    'o',
    'process',
    '<process>',
    'name of the process',
    's'
);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'minimal',
    '',
    'n',
    'minimal',
    '<threshold>',
    'minimal threshold, number of process',
    'i'
);
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'args',
    '',
    'r',
    'args',
    'false',
    'check also args for process matching, default: no',
    ''
);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);

# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in Snmp parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_thresholds(\%options, \%parameters)) {

    print "\nMistake in warning/critical parameters: critical must be > warning\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'process')) {

    print "\nMissing process name : -o|--process\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}


# Work on snmp
my %process_list = ();

if(check_parameter(\%options, \%parameters, 'args')) {

    write_debug_if_requested(\%options, "OID: $oids{'list_process'}");
    write_debug_if_requested(\%options, "OID: $oids{'list_args'}");

    %process_list = walk_two_oids(\%options, \%parameters, $oids{'list_process'}, $oids{'list_args'});
}
else {

    write_debug_if_requested(\%options, "OID: $oids{'list_process'}");

    %process_list = walk_oid(\%options, \%parameters, $oids{'list_process'});
}

if(not (test_snmp_values(\%options, \%process_list))) {

    write_plugin_output(\%options, $state, "Can not get process list\n");
    exit($ERRORS{"$state"});
}


# look for requested process
$results{'process_number'} = 0;
foreach my $value (values(%process_list)) {

    chomp($value);

    write_debug_if_requested(\%options, "current process: $value");

    if($value =~ /$parameters{'process'}{'value'}/ and $value !~ /$options{'name'}/) {

        $results{'process_number'}++;
        write_debug_if_requested(\%options, 'match here');
    }
}

write_debug_if_requested(\%options, "number of process found: $results{'process_number'}");


# Different cases of exit
if(defined($parameters{'minimal'}{'value'}) and $results{'process_number'} < $parameters{'minimal'}{'value'}) {

    $state = 'CRITICAL';
}
elsif(defined($parameters{'critical'}{'value'}) and $results{'process_number'} > $parameters{'critical'}{'value'}) {

    $state = 'CRITICAL';
}
elsif(defined($parameters{'warning'}{'value'}) and $results{'process_number'} > $parameters{'warning'}{'value'}) {

    $state = 'WARNING';
}
else {

    $state = 'OK';
}

# Print exit of plugin
my $msg = "$results{'process_number'} found";

if(defined($parameters{'minimal'}{'value'})) {

    $msg .= " with minimum number of $parameters{'minimal'}{'value'} needed";
}

write_plugin_output(\%options, $state, "$msg");

if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();
    $perfData{'process_'.$parameters{'process'}{'value'}}{'value'} = $results{'process_number'};

    if(defined($parameters{'warning'}{'value'})) {

        $perfData{'process_'.$parameters{'process'}{'value'}}{'warning'} = "$parameters{'warning'}{'value'}";
    }

    if(defined($parameters{'critical'}{'value'})) {

        $perfData{'process_'.$parameters{'process'}{'value'}}{'critical'} = "$parameters{'critical'}{'value'}";
    }

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

