

=head1 general informations

Description:
    This package contains all SNMP functions for glz-monitoring-plugins a.k.a. gmp

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Name of this package
package Gmp::Snmp;


## Standard includes
use strict;
use warnings;
require Exporter;
use Net::SNMP qw(oid_base_match);


## Gmp includes
use Gmp::Common;


## Exports
our $VERSION    = 'master';
our @ISA        = qw(Exporter);
our @EXPORT     = qw(
    get_oid
    walk_oid
    walk_two_oids
    test_snmp_value
    test_snmp_values
    found_snmp_disk_id
    found_snmp_interface_id
    is_interface_up
    build_snmp_disk_list
);



=head1 start_snmp_session()

start SNMP session
Parameters:
    - hash table of options
    - hash table of parameters

start_snmp_session(\%options, \%parameters)

=cut

sub start_snmp_session {

    my($ref_options, $ref_parameters) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my($session, $error) = undef;

    # Start SNMP session
    if( $parameters{'protocol'}{'value'} eq '1'
     or $parameters{'protocol'}{'value'} eq '2') {

        write_debug_if_requested(\%options, 'v1 or v2 session');

        ($session, $error) = Net::SNMP->session(
            -hostname   => $parameters{'host'}{'value'},
            -version    => $parameters{'protocol'}{'value'},
            -port       => $parameters{'port'}{'value'},
            -domain     => $parameters{'transport'}{'value'},
            -timeout    => $parameters{'timeout'}{'value'},
            -community  => $parameters{'community'}{'value'},
            );
    }
    elsif($parameters{'protocol'}{'value'} eq '3') {

        write_debug_if_requested(\%options, 'v3 session');

        ($session, $error) = Net::SNMP->session(
            -hostname       => $parameters{'host'}{'value'},
            -port           => $parameters{'port'}{'value'},
            -domain         => $parameters{'transport'}{'value'},
            -timeout        => $parameters{'timeout'}{'value'},
            -version        => $parameters{'protocol'}{'value'},
            -username       => $parameters{'username'}{'value'},
            -authpassword   => $parameters{'authpass'}{'value'},
            -authprotocol   => $parameters{'authproto'}{'value'},
            -privpassword   => $parameters{'privpass'}{'value'},
            -privprotocol   => $parameters{'privproto'}{'value'},
            );
    }
    else {

        write_debug_if_requested(\%options, 'unknown session version');
    }

    write_debug_if_requested(\%options, 'session can not be created') if(not defined($session));

    return($session);
}

=pod

Return the snmp session.
Test it !

=cut



=head1 get_oid()

Get the value of a particular OID
Parameters:
    - hash table of options
    - hash table of parameters
    - OID to get

get_oid(\%options, \%parameters, $oid)

=cut

sub get_oid {

    my($ref_options, $ref_parameters, $oid) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);
    write_debug_if_requested(\%options, "oid to get: $oid");

    my($result, $value) = undef;

    my $session = start_snmp_session(\%options, \%parameters);

    if(not defined($session)) {

        write_debug_if_requested(\%options, 'missing SNMP session');
        return $result;
    }

    # Query the OID
    $value  = $session->get_request(-varbindlist=>[$oid]);
    $result = $value->{$oid} if(defined($value)); 

    # if SNMP get problem
    write_debug_if_requested(\%options, "error reading $oid on $parameters{'host'}{'value'}:$parameters{'port'}{'value'} with $parameters{'community'}{'value'}") if(not defined($result));

    $session->close();
    return($result);
}

=pod

Return the value or undef in case of problem
Test it with test_snmp_value()

=cut



=head1 walk_oid()

Walk an OID
Parameters:
    - hash table of options
    - hash table of parameters
    - OID to walk

walk_oid(\%options, \%parameters, $oid)

=cut

sub walk_oid {

    my($ref_options, $ref_parameters, $oid) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $session = start_snmp_session(\%options, \%parameters);
    my %results = ();
    
    if(not defined($session)) {

        write_debug_if_requested(\%options, 'missing SNMP session');
        return %results;
    }

    # Init
    write_debug_if_requested(\%options, "oid to walk: $oid");

    # Query the OID
    my $res = undef;
    if($parameters{'protocol'}{'value'} eq '2'
    or $parameters{'protocol'}{'value'} eq '3') {

        $res = $session->get_table(
            -baseoid=>$oid,
            -maxrepetitions=>1,
        );
    }
    else{

        $res = $session->get_table(
            -baseoid=>$oid,
        );
    }

    if($res) {
        %results = %{$res};
    }

    # if SNMP get problem
    write_debug_if_requested(\%options, "error reading $oid on $parameters{'host'}{'value'}:$parameters{'port'}{'value'} with $parameters{'community'}{'value'}") if(scalar(keys(%results)) <= 0);

    $session->close;
    return(%results);
}

=pod

Return a hash of value(s) or an empty hash
Test it with test_snmp_values()

=cut



=head1 walk_two_oids()

Walk two OIDs in the same time and aggregate the results
Parameters:
- hash table of options
- hash table of parameters
- first OID to walk
- second OID to walk

walk_two_oids(\%options, \%parameters, $oid1, $oid2)

=cut

sub walk_two_oids {

    my($ref_options, $ref_parameters, $oid, $oid2) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my %results = ();

    # Init
    my $current_oid = $oid;
    write_debug_if_requested(\%options, "first oid to walk: $oid");
    write_debug_if_requested(\%options, "second oid to walk: $oid2");

    # Query the OID
    my %res1    = walk_oid(\%options, \%parameters, $oid);
    my %res2    = walk_oid(\%options, \%parameters, $oid2);

    # Build results
    while (my ($key,$value) = each (%res1)) {

        my @splitedOid = split(/\./, $key);
        my $id = pop(@splitedOid);
        my $fullOid2 = "${oid2}.${id}";
        my $value2 = $res2{$fullOid2};
        if(defined($value2)) {
            $results{$id} = "$value $value2";
        }
        else {
            $results{$id} = $value;
        }
    }

    # if SNMP get problem
    write_debug_if_requested(\%options, "error reading $oid and $oid2 on $parameters{'host'}{'value'}:$parameters{'port'}{'value'} with $parameters{'community'}{'value'}") if(scalar(keys(%results)) <= 0);

    return(%results);
}

=pod

Return a hash of value(s) or an empty hash
Test it with test_snmp_values()

=cut



=head1 test_snmp_value()

Test a SNMP result
To be used after get_oid()
Parameters:
    - hash table of options
    - value to test

test_snmp_value(\%options, $value)

=cut

sub test_snmp_value {

    my($ref_options, $value) = @_;
    my %options = %{$ref_options};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    if(defined($value)) {

        write_debug_if_requested(\%options, 'valid snmp value');

        $result = 1;
    }
    else {

        write_debug_if_requested(\%options, 'not valid snmp value');

        $result = 0;
    }

    return($result);
}

=pod

Return "true" if value is valid

=cut



=head1 test_snmp_values()

Test SNMP results
To be used after walk_oid() or walk_two_oids()
Parameters:
    - hash table of options
    - hash table of results to test

test_snmp_values(\%options, \%values)

=cut

sub test_snmp_values {

    my($ref_options, $ref_values) = @_;
    my %options = %{$ref_options};
    my %values  = %{$ref_values};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    if(scalar(keys(%values)) > 0) {

        write_debug_if_requested(\%options, 'valid snmp values');

        $result = 1;
    }
    else {

        write_debug_if_requested(\%options, 'not valid snmp values');

        $result = 0;
    }

    return($result);
}

=pod

Return "true" if at least one valid value

=cut



=head1 found_snmp_disk_id()

Find the SNMP id of a partition
Parameters:
    - hash table of options
    - hash table of parameters
    - List OID to walk
    - partition to find

found_snmp_disk_id(\%options, \%parameters, $oid, $disk)

=cut

sub found_snmp_disk_id {

    my($ref_options, $ref_parameters, $oid, $disk) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $disk_id = undef;

    # Build the compare string
    my $compareString = $disk;
    $compareString =~ s/\//\\\//g;
    $compareString = "^".$compareString."(,|\$)";

    # get the list of partition
    my %partitions = walk_oid(\%options, \%parameters, $oid);
    
    test_snmp_values(\%options, \%partitions);

    DISKS:
    while(my($oid, $name) = each(%partitions)) {

        write_debug_if_requested(\%options, "current line: $oid : $name");

        if($name =~ /$compareString/) {

            write_debug_if_requested(\%options, "found requested partition: $disk");

            # Get ID of the partition
            my @tmp = split(/\./, $oid);
            $disk_id = pop(@tmp);

            write_debug_if_requested(\%options, "id: $disk_id");

            last DISKS;
        }
    }
    return($disk_id);
}

=pod

Return id of requested disk

=cut



=head1 found_snmp_interface_id()

Function to find the SNMP id of a partition
Parameters:
    - hash table of options
    - hash table of parameters
    - List OID to walk
    - interface to find

found_snmp_interface_id(\%options, \%parameters, $oid, $interface)

=cut

sub found_snmp_interface_id {

    my($ref_options, $ref_parameters, $oid, $interface) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $interface_id = undef;

    # Build the compare string
    my $compareString = $interface;
    $compareString =~ s/\//\\\//g;
    $compareString = "^$compareString\$";

    # get the list of partition
    my %interfaces = walk_oid(\%options, \%parameters, $oid);

    test_snmp_values(\%options, \%interfaces);

    INTERFACES:
    while(my($oid, $name) = each(%interfaces)) {

        write_debug_if_requested(\%options, "current line: $oid : $name");

        if($name =~ /$compareString/) {

            write_debug_if_requested(\%options, "found requested interface: $interface");

            # Get ID of the interface
            my @tmp = split(/\./, $oid);
            $interface_id = pop(@tmp);

            write_debug_if_requested(\%options, "id: $interface_id");

            last INTERFACES;
        }
    }
    return($interface_id);
}

=pod

Return id of requested interface

=cut



=head1 is_interface_up()

Function to check if interface is up or not
Parameters:
    - hash table of options
    - hash table of parameters
    - interface name
    - interface to find

is_interface_up(\%options, \%parameters, $oid)

=cut

sub is_interface_up {

    my($ref_options, $ref_parameters, $oid) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $result = undef;

    # get status of interface
    my $status = get_oid(\%options, \%parameters, $oid);

    if(not (test_snmp_value(\%options, $status))) {

        write_debug_if_requested(\%options, 'can not get interface status');
    }
    elsif($status == '1') {

        write_debug_if_requested(\%options, 'interface is up');

        $result = 1;
    }
    else {

        $result = 0;
    }

    return($result);
}

=pod

Return "true" if interface is up and false if down
also return undef if no information

=cut



=head1 build_snmp_disk_list()

Find the SNMP id of partitions
Parameters:
    - hash table of options
    - hash table of parameters
    - List OID to walk

found_snmp_disk_id(\%options, \%parameters, $oid, $disk)

=cut

sub build_snmp_disk_list {

    my($ref_options, $ref_parameters, $oid) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $disk_id = undef;

    # Work on exclude list
    my @excludes = ();
    if(check_parameter(\%options, \%parameters, 'exclude')) {

        @excludes = split(/,/, $parameters{'exclude'}{'value'});
    }

    # get the list of partition
    my %partitions = walk_oid(\%options, \%parameters, $oid);
    my %my_partitions = ();
    
    test_snmp_values(\%options, \%partitions);

    DISKS:
    while(my($oid, $name) = each(%partitions)) {

        write_debug_if_requested(\%options, "current partition: $name - OID: $oid");

        if($name !~ /^\// and $name !~ /^[A-Z]:\\\sLabel:/) {

            # only work on real parition disk
            # beginning with '/'
            write_debug_if_requested(\%options, "Excluded by real partition check !");
            next;
        }
        elsif(  $name =~ /^\/dev/ or
                $name =~ /^\/sys/ or
                $name =~ /^\/proc/ or
                $name =~ /^\/run\/lxcfs\/controllers/ or
                $name =~ /^\/var\/lib\/ceph\/osd\/ceph/ or
                $name =~ /^\/var\/lib\/nfs\/rpc_pipefs/ or
                $name =~ /\.snapshot/) {

            # exclude some partitions not disk !
            # example: /dev
            write_debug_if_requested(\%options, "Excluded by disk partition check !");
            next;
        }

        # work with user exclusions
        my $found = 0;
        foreach my $ex (@excludes) {

            my $exclude = $ex;
            $exclude =~ s/\//\\\//g;
            $exclude = "^".$exclude."(,|\$)";
            write_debug_if_requested(\%options, "Exclude string: $exclude");
            if($name =~ /$exclude/) {

                $found = 1;
            }
        }
        if($found) {

            # found in exclude list
            write_debug_if_requested(\%options, "Excluded by exclude list !");
            next;
        }

        # If we reach here:
        # Add partition id to list
        my @oidTab = split(/\./,$oid);
        if($name =~ /^([A-Z]:)\\\sLabel:(\w+)\s+/) {

            # Special job for windows name
            $my_partitions{"$1 ($2)"} = pop(@oidTab);
        }
        elsif($name =~ /^([A-Z]:)\\\sLabel:\s+/) {

            # Special job for windows name
            $my_partitions{"$1 (no label)"} = pop(@oidTab);
        }
        else {
            $my_partitions{$name} = pop(@oidTab);
        }
    }
    return(%my_partitions);
}

=pod

Return hash table with disk name and id

=cut



1;
