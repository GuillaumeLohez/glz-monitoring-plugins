

=head1 general informations

Description:
    This package contains all common functions for glz-monitoring-plugins a.k.a. gmp

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Name of this package
package Gmp::Common;


## Standard includes
use strict;
use warnings;
require Exporter;
use Getopt::Long;
use File::Basename;
use Number::Format qw(unformat_number);


## Exports
our $VERSION    = 'master';
our @ISA        = qw(Exporter);
our @EXPORT     = qw(
    %ERRORS
    build_standard_snmp_parameters
    get_parameters_value
    check_parameter
    print_version
    print_help
    print_usage
    check_snmp_parameters
    check_thresholds
    calculate_percent
    add_parameter
    read_cache_file
    update_cache_file
    read_previous_file
    update_previous_file
    write_plugin_output
    write_debug_if_requested
    write_plugin_perfdata 
    clean_name
    check_parameter_percent
);



=head1 common variables

%ERRORS with standard error codes

=cut

our %ERRORS = (
    'OK'=>0,
    'WARNING'=>1,
    'CRITICAL'=>2,
    'UNKNOWN'=>3,
    'DEPENDENT'=>4,
);


=head1 build_standard_snmp_parameters()

Function to build all standard parameters for SNMP plugin
Parameters:
    - hash table of options

build_standard_snmp_parameters(\%options)

=cut

sub build_standard_snmp_parameters {

    my($ref_options) = @_;
    my %options = %{$ref_options};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my %parameters = ();

    $parameters{'version'}{'value'}         = undef;
    $parameters{'version'}{'shortName'}     = 'v';
    $parameters{'version'}{'longName'}      = 'version';
    $parameters{'version'}{'shortDesc'}     = 'false';
    $parameters{'version'}{'longDesc'}      = 'Display current version';
    $parameters{'version'}{'type'}          = undef;

    $parameters{'help'}{'value'}            = undef;
    $parameters{'help'}{'shortName'}        = 'h';
    $parameters{'help'}{'longName'}         = 'help';
    $parameters{'help'}{'shortDesc'}        = 'false';
    $parameters{'help'}{'longDesc'}         = 'Display help';
    $parameters{'help'}{'type'}             = undef;

    $parameters{'host'}{'value'}            = undef;
    $parameters{'host'}{'shortName'}        = 'H';
    $parameters{'host'}{'longName'}         = 'host';
    $parameters{'host'}{'shortDesc'}        = '<ip address>';
    $parameters{'host'}{'longDesc'}         = 'IP or hostname to query';
    $parameters{'host'}{'type'}             = 's';

    $parameters{'port'}{'value'}            = 161;
    $parameters{'port'}{'shortName'}        = 'p';
    $parameters{'port'}{'longName'}         = 'port';
    $parameters{'port'}{'shortDesc'}        = '<161>';
    $parameters{'port'}{'longDesc'}         = 'SNMPd remote port (Default: 161)';
    $parameters{'port'}{'type'}             = 'i';

    $parameters{'timeout'}{'value'}         = 10;
    $parameters{'timeout'}{'shortName'}     = 't';
    $parameters{'timeout'}{'longName'}      = 'timeout';
    $parameters{'timeout'}{'shortDesc'}     = '<10>';
    $parameters{'timeout'}{'longDesc'}      = 'SNMP request timeout (Default: 10)';
    $parameters{'timeout'}{'type'}          = 'i';

    $parameters{'protocol'}{'value'}        = '2';
    $parameters{'protocol'}{'shortName'}    = 'P';
    $parameters{'protocol'}{'longName'}     = 'protocol';
    $parameters{'protocol'}{'shortDesc'}    = '<2>';
    $parameters{'protocol'}{'longDesc'}     = 'SNMP request protocol 1, 2 or 3 (Default: 2)';
    $parameters{'protocol'}{'type'}         = 's';

    $parameters{'community'}{'value'}       = 'public';
    $parameters{'community'}{'shortName'}   = 'C';
    $parameters{'community'}{'longName'}    = 'community';
    $parameters{'community'}{'shortDesc'}   = '<public>';
    $parameters{'community'}{'longDesc'}    = 'SNMP request community for v1 or v2 (Default: public)';
    $parameters{'community'}{'type'}        = 's';

    $parameters{'username'}{'value'}        = undef;
    $parameters{'username'}{'shortName'}    = 'u';
    $parameters{'username'}{'longName'}     = 'username';
    $parameters{'username'}{'shortDesc'}    = '<username>';
    $parameters{'username'}{'longDesc'}     = 'SNMP request username for v3';
    $parameters{'username'}{'type'}         = 's';

    $parameters{'authpass'}{'value'}        = undef;
    $parameters{'authpass'}{'shortName'}    = 'A';
    $parameters{'authpass'}{'longName'}     = 'authpass';
    $parameters{'authpass'}{'shortDesc'}    = '<authpass>';
    $parameters{'authpass'}{'longDesc'}     = 'SNMP request auth password for v3';
    $parameters{'authpass'}{'type'}         = 's';

    $parameters{'authproto'}{'value'}       = undef;
    $parameters{'authproto'}{'shortName'}   = 'a';
    $parameters{'authproto'}{'longName'}    = 'authproto';
    $parameters{'authproto'}{'shortDesc'}   = '<authproto>';
    $parameters{'authproto'}{'longDesc'}    = 'SNMP request auth protocol for v3, MD5 or SHA';
    $parameters{'authproto'}{'type'}        = 's';

    $parameters{'privpass'}{'value'}        = undef;
    $parameters{'privpass'}{'shortName'}    = 'X';
    $parameters{'privpass'}{'longName'}     = 'privpass';
    $parameters{'privpass'}{'shortDesc'}    = '<privpass>';
    $parameters{'privpass'}{'longDesc'}     = 'SNMP request priv password for v3';
    $parameters{'privpass'}{'type'}         = 's';

    $parameters{'privproto'}{'value'}       = undef;
    $parameters{'privproto'}{'shortName'}   = 'x';
    $parameters{'privproto'}{'longName'}    = 'privproto';
    $parameters{'privproto'}{'shortDesc'}   = '<privproto>';
    $parameters{'privproto'}{'longDesc'}    = 'SNMP request priv protocol for v3, DES or AES';
    $parameters{'privproto'}{'type'}        = 's';

    $parameters{'transport'}{'value'}       = 'udp4';
    $parameters{'transport'}{'shortName'}   = 'T';
    $parameters{'transport'}{'longName'}    = 'transport';
    $parameters{'transport'}{'shortDesc'}   = '<udp4>';
    $parameters{'transport'}{'longDesc'}    = 'Transport protocal udp4 or udp6 (Default: udp4)';
    $parameters{'transport'}{'type'}        = 's';

    $parameters{'warning'}{'value'}         = undef;
    $parameters{'warning'}{'shortName'}     = 'w';
    $parameters{'warning'}{'longName'}      = 'warning';
    $parameters{'warning'}{'shortDesc'}     = '<threshold>';
    $parameters{'warning'}{'longDesc'}      = 'warning threshold(s)';
    $parameters{'warning'}{'type'}          = 's';

    $parameters{'critical'}{'value'}        = undef;
    $parameters{'critical'}{'shortName'}    = 'c';
    $parameters{'critical'}{'longName'}     = 'critical';
    $parameters{'critical'}{'shortDesc'}    = '<threshold>';
    $parameters{'critical'}{'longDesc'}     = 'critical threshold(s)';
    $parameters{'critical'}{'type'}         = 's';

    $parameters{'perfdata'}{'value'}        = undef;
    $parameters{'perfdata'}{'shortName'}    = 'f';
    $parameters{'perfdata'}{'longName'}     = 'perfdata';
    $parameters{'perfdata'}{'shortDesc'}    = 'false';
    $parameters{'perfdata'}{'longDesc'}     = 'enable perfdata output';
    $parameters{'perfdata'}{'type'}         = '';

    return(%parameters);
}

=pod

Return hash table with all parameters and their default values

=cut



=head1 get_parameters_value()

Get value from all parameters
Parameters:
    - hash table of options
    - hash table of parameters
    - @ARGV from main script which call this function

get_parameters_value(\%options, \%parameters, \@ARGV)

=cut

sub get_parameters_value {

    my($ref_options, $ref_parameters) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    # For GetOptions to work
    my $toRemove    = shift;
    $toRemove       = shift;
    my @ARGV        = @_;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my %tmpParams = ();

    PARAMETERS:
    foreach my $key (keys(%parameters)) {

        write_debug_if_requested(\%options, "work on key: $key");
        
        if($parameters{"$key"}{'type'}) {

            write_debug_if_requested(\%options, 'short: '.$parameters{"$key"}{'shortName'}.", ".$parameters{"$key"}{'longName'}.", ".$parameters{"$key"}{'type'});

            $tmpParams{$parameters{"$key"}{'shortName'}."|".$parameters{"$key"}{'longName'}."=".$parameters{"$key"}{'type'}} = \$parameters{"$key"}{'value'}; 
        }
        else {

            write_debug_if_requested(\%options, 'short: '.$parameters{"$key"}{'shortName'}.", ".$parameters{"$key"}{'longName'}.", boolean");

            $tmpParams{$parameters{"$key"}{'shortName'}."|".$parameters{"$key"}{'longName'}} = \$parameters{"$key"}{'value'}; 
        }
    }

    Getopt::Long::Configure('no_ignorecase');
    GetOptions(%tmpParams);

    return(%parameters);
}

=pod

Return hash table with real values

=cut



=head1 check_parameter()

Check if a parameter is defined or not
Parameters:
    - hash table of options
    - hash table of parameters
    - name of parameter to check

check_parameter(\%options, \%parameters, $parameter)

=cut

sub check_parameter {

    my($ref_options, $ref_parameters, $parameter) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);
    write_debug_if_requested(\%options, "checking paramater: $parameter");

    if(defined($parameters{"$parameter"}{'value'})) {

        $result = 1;
    }
    else {

        $result = 0;
    }

    return($result);
}

=pod

Return "true" or "false" depending of the parameter value

=cut



=head1 print_version()

Print version and fancy output
Parameters:
    - hash table of options

print_version(\%options)

=cut

sub print_version {

    my($ref_options) = @_;
    my %options = %{$ref_options};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    print "\n";
    print "$options{'name'} : $options{'details'}\n";
    print "Version: $options{'version'}\n";
    print "\n";
}

=pod

Print plugin version with fancy message

=cut



=head1 print_help()

Print help and fancy output
Parameters:
    - hash table of options
    - hash table of parameters

print_version(\%options, \%parameters)

=cut

sub print_help {

    my($ref_options, $ref_parameters) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    print "\n" ;
    print "$options{'name'} : $options{'details'}\n";
    print "\n";
    print "Options:\n";

    PARAMETERS:
    foreach my $key (sort { $parameters{$a}{'longName'} cmp $parameters{$b}{'longName'} } keys %parameters) {

        write_debug_if_requested(\%options, "current key: $key");
        
        printf("%-12s: -%-1s|--%-12s : %-40s\n", $key, $parameters{"$key"}{'shortName'}, $parameters{"$key"}{'longName'}, $parameters{"$key"}{'longDesc'});
    }

    print "\n";
}

=pod

Print plugin help with fancy message

=cut



=head1 print_usage()

Print usage and fancy output
Parameters:
    - hash table of options
    - hash table of parameters

print_usage(\%options, \%parameters)

=cut

sub print_usage {

    my($ref_options, $ref_parameters) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    print "Usage: $options{'name'}\n";

    PARAMETERS:
    foreach my $key (sort { $parameters{$a}{'longName'} cmp $parameters{$b}{'longName'} } keys %parameters) {

        write_debug_if_requested(\%options, "current key: $key");
        
        printf("%-12s: -%-1s|--%-12s : %-40s\n", $key, $parameters{"$key"}{'shortName'}, $parameters{"$key"}{'longName'}, $parameters{"$key"}{'shortDesc'});
    }

    print "\n";
    print "Use -h or --help for extended help\n";
    print "\n";
}

=pod

Print plugin usage with fancy message

=cut



=head1 check_snmp_parameters()

Check SNMP parameters consistency
Parameters:
    - hash table of options
    - hash table of parameters

check_snmp_parameters(\%options, \%parameters)

=cut

sub check_snmp_parameters {

    my($ref_options, $ref_parameters) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    if( ($parameters{'protocol'}{'value'} eq '1' or $parameters{'protocol'}{'value'} eq '2c')
        and not defined($parameters{'community'}{'value'})) {

        $result = 1;
    }
    elsif (($parameters{'protocol'}{'value'} eq '3') and (
        not defined($parameters{'username'}{'value'})
        or not defined($parameters{'authpass'}{'value'})
        or not defined($parameters{'authproto'}{'value'})
        or not defined($parameters{'privpass'}{'value'})
        or not defined($parameters{'privproto'}{'value'})
    )) {

        $result = 1;
    }
    else {

        $result = 0;
    }

    return($result);
}

=pod

Return "true" in case of problem

=cut



=head1 check_thresholds()

Check thresholds parameters consistency
Parameters:
    - hash table of options
    - hash table of parameters

check_thresholds(\%options, \%parameters)

=cut

sub check_thresholds {

    my($ref_options, $ref_parameters) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    if(defined($parameters{'warning'}{'value'}) and defined($parameters{'critical'}{'value'})){

        # do we have number only ?
        my $warn = undef;
        if(($parameters{'warning'}{'value'} !~ /^\d+$/)){

            $warn = unformat_number($parameters{'warning'}{'value'}, base => 1024);
        }
        else {

            $warn = $parameters{'warning'}{'value'};
        }

        my $crit = undef;
        if(($parameters{'critical'}{'value'} !~ /^\d+$/)){

            $crit = unformat_number($parameters{'critical'}{'value'}, base => 1024);
        }
        else {

            $crit = $parameters{'critical'}{'value'};
        }
    
        if($warn >= $crit) {

            $result = 1;
        }
        else {

            $result = 0;
        }
    }
    else {

        $result = 0;
    }

    return($result);
}

=pod

Return "true" in case of problem

=cut



=head1 calculate_percent()

Calculate percent between used and total
Parameters:
    - hash table of options
    - used
    - total

calculate_percent(\%options, $used, $total)

=cut

sub calculate_percent {

    my($ref_options, $used, $total) = @_;
    my %options     = %{$ref_options};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    my $percent = undef;

    if(defined($used) and $used =~ /^\d+$/
        and defined($total) and $total =~ /^\d+$/ and $total > 0 ) {

        $percent = int($used * 100 / $total) + 1;
    }

    return($percent);
}

=pod

Return the percent calculated

=cut



=head1 add_parameter()

Add a custom parameter
Parameters:
    - hash table of options
    - hash table of parameters
    - name of parameter
    - default value of parameter
    - short option of parameter
    - long option of parameter
    - short description of parameter
    - long description of parameter
    - type of parameter

add_parameter(\%options, \%parameters, $name, $value, $shortName, $longName, $shortDesc, $longDesc, $type)

=cut

sub add_parameter {

    my($ref_options, $ref_parameters, $name, $value, $shortName, $longName, $shortDesc, $longDesc, $type) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);
    write_debug_if_requested(\%options, "add parameter: $name with $shortName and $longName");

    if(defined($value) and $value ne '') {

        $parameters{"$name"}{'value'}   = "$value";
    }
    else {

        $parameters{"$name"}{'value'}   = undef;
    }

    $parameters{"$name"}{'shortName'}   = $shortName;
    $parameters{"$name"}{'longName'}    = $longName;
    $parameters{"$name"}{'shortDesc'}   = $shortDesc;
    $parameters{"$name"}{'longDesc'}    = $longDesc;
    if($type) {

        $parameters{"$name"}{'type'}    = $type;
    }
    else {

        $parameters{"$name"}{'type'}    = undef;
    }

    return(%parameters);
}

=pod

Return hash table plus new parameter with its values

=cut



=head1 read_cache_file()

Get the index value in a cache file
Parameters:
    - hash table of options
    - cache file to read

read_cache_file(\%options, $cache_file)

=cut

sub read_cache_file {

    my($ref_options, $cache_file) = @_;
    my %options = %{$ref_options};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);
    
    if(-e $cache_file) {

        my $mtime = (stat($cache_file))[9];

        if((time - $mtime) < $options{'lifetime'}) {

            write_debug_if_requested(\%options, 'use value from cache file');
            
            # Read value
            open(CACHE_FILE, "<$cache_file");
            $result = <CACHE_FILE>;
            chomp($result);
            close(CACHE_FILE);
        }
    }
    else {

        write_debug_if_requested(\%options, "can not read cache file: $cache_file");
    }

    return($result);
}

=pod

Return the id

=cut



=head1 update_cache_file()

Update the index value in a cache file
Parameters:
    - hash table of options
    - cache file to update
    - value to write

update_cache_file(\%options, $cache_file, $value)

=cut

sub update_cache_file {

    my($ref_options, $cache_file, $value) = @_;
    my %options = %{$ref_options};

    my $dir = dirname("$cache_file");
    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    # Delete cache file
    if(not -w $cache_file) {

        write_debug_if_requested(\%options, "can not write access: $dir");
        $result = 0;
    }
    elsif(-e $cache_file) {

        write_debug_if_requested(\%options, "delete cache file: $cache_file");
        unlink($cache_file);
    }
    else {

        write_debug_if_requested(\%options, "no cache file: $cache_file");
    }

    if(-w $dir) {

        # Write cache file
        write_debug_if_requested(\%options, 'write cache file');

        open(CACHE_FILE, ">$cache_file");
        print CACHE_FILE $value;
        close(CACHE_FILE);

        $result = 1;
    }
    else {

        write_debug_if_requested(\%options, "can not write: $cache_file");

        $result = 0;
    }

    return($result);
}

=pod 

Return "true" for success

=cut



=head1 read_previous_file()

Read previous values and insert them in hash table
Parameters:
    - hash table of options
    - previous file to read
    - hash table with results

read_previous_file(\%options, $previous_file, \%results)

=cut

sub read_previous_file {

    my($ref_options, $previous_file, $ref_results) = @_;
    my %options     = %{$ref_options};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);
    
    if(-e $previous_file) {

        write_debug_if_requested(\%options, "reading previous file: $previous_file");
        open(PREVIOUS_FILE, "<$previous_file");

        FILE:
        foreach my $line (<PREVIOUS_FILE>) {

            chomp($line);
            write_debug_if_requested(\%options, "current line: $line");
            
            if($line =~ /^(\w+)=(\d+)$/) {

                my $realKey = "previous_$1";
                my $value = $2;

                write_debug_if_requested(\%options, "Current real key: $realKey with value $value");

                $ref_results->{"$realKey"} = $value;
            }
        }

        close(PREVIOUS_FILE);

        $result = 1;
    }
    else {

        write_debug_if_requested(\%options, "can not read: $previous_file");

        $result = 0;
    }

    return($result);
}

=pod 

Return "true" for success

=cut



=head1 update_previous_file()

Update the previous values in file from hash table
Parameters:
    - hash table of options
    - previous file to read
    - hash table with results

update_previous_file(\%options, $previous_file, \%results)

=cut

sub update_previous_file {

    my($ref_options, $previous_file, $ref_results) = @_;
    my %options     = %{$ref_options};
    my %results     = %{$ref_results};

    my $dir = dirname("$previous_file");
    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    # Delete previous file
    if(not -w $previous_file) {

        write_debug_if_requested(\%options, "can not write: $previous_file");

        $result = 0;
    }
    elsif(-e $previous_file) {

        write_debug_if_requested(\%options, "delete previous file: $previous_file");

        unlink($previous_file);
    }
    else {

        write_debug_if_requested(\%options, "no previous file: $previous_file");
    }

    if(-w $dir) {

        # Write previous file
        write_debug_if_requested(\%options, "write: $previous_file");

        open(PREVIOUS_FILE, ">$previous_file");

        RESULTS:
        foreach my $key (keys(%results)) {

            if($key =~ /^previous_/) {

                write_debug_if_requested(\%options, "Current key: $key");
                my $realKey = lcfirst(substr($key, 9));

                write_debug_if_requested(\%options, "Current real key: $realKey");

                print(PREVIOUS_FILE "$realKey=".$results{"$realKey"}."\n");
            }
        }

        close(PREVIOUS_FILE);

        $result = 1;
    }
    else {

        write_debug_if_requested(\%options, "can not write: $previous_file");

        $result = 0;
    }
    
    return($result);
}

=pod

Return "true" for success

=cut



=head1 write_plugin_output()

Write plugin output in standard way
Parameters:
    - hash table of options
    - state
    - message

write_plugin_output(\%options, $state, $message);

=cut

sub write_plugin_output {

    my($ref_options, $state, $message) = @_;
    my %options = %{$ref_options};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    print "$options{'display'} $state - $message";
}

=pod

Return nothing... This is just a print

=cut



=head1 write_debug_if_requested()

Write debug information if $options{'debug'} is true
Parameters:
    - hash table of options
    - message

write_debug_if_requested(\%options, $message);

=cut

sub write_debug_if_requested {

    my($ref_options, $message) = @_;
    my %options = %{$ref_options};

    if($options{'debug'}) {

        print "**debug** $message\n";
    }
}

=pod

Return nothing... This is just a print

=cut



=head1 write_plugin_perfdata()

Write plugin perfdata
Parameters:
    - hash table of options
    - hash table with perf data

write_plugin_perfdata(\%options, \%perfdata);

=cut

sub write_plugin_perfdata {

    my($ref_options, $ref_perfdata) = @_;
    my %options     = %{$ref_options};
    my %perfdata    = %{$ref_perfdata};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    print " |";

    PERFDATA:
    foreach my $key (keys(%perfdata)) {

        write_debug_if_requested(\%options, "Working with key: $key");

        print " $key=".$perfdata{"$key"}{'value'};
        foreach my $k ('warning', 'critical', 'min', 'max') {

            write_debug_if_requested(\%options, "Working with value: $k");

            if(defined($perfdata{"$key"}{"$k"})) {

                print ";".$perfdata{"$key"}{"$k"};
            }
            else {

                print ";";
            }
        }
    }

}

=pod

Return nothing... This is just a few prints

=cut


=head1 clean_name()

Clean name of ' ' '/' for cache or previous file
Parameters:
    - hash table of options
    - name to clean

clean_name(\%options, $name)

=cut

sub clean_name {

    my($ref_options, $name) = @_;
    my %options     = %{$ref_options};

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);

    # replace ' ' with '_'
    $name=~ s/\s+/_/g;

    # replace '/' with '_'
    $name=~ s!/!_!g;

    return($name);
}

=pod

Return the name cleanned

=cut


=head1 check_parameter_percent()

Check if a parameter is in percent or not
Parameters:
    - hash table of options
    - hash table of parameters
    - name of parameter to check

check_parameter_percent(\%options, \%parameters, $parameter)

=cut

sub check_parameter_percent {

    my($ref_options, $ref_parameters, $parameter) = @_;
    my %options     = %{$ref_options};
    my %parameters  = %{$ref_parameters};

    my $result = undef;

    write_debug_if_requested(\%options, 'in function: '.(caller(0))[3]);
    write_debug_if_requested(\%options, "checking paramater: $parameter");

    if( defined($parameters{"$parameter"}{'value'})
        and $parameters{"$parameter"}{'value'} >= 0
        and $parameters{"$parameter"}{'value'} <= 100
      ) {

        $result = 1;
    }
    else {

        $result = 0;
    }

    return($result);
}

=pod

Return "true" or "false" depending of the parameter value

=cut


1;
