#!/usr/bin/perl


=head1 general informations

Description:
    This plugin check load average of a linux server.
    It uses Net-SNMP MIB.

Creation:
    March 2014

Author:
    Guillaume Lohez
    gmp@glz.io
    http://glz.io

License:
    GPLv2 - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

=cut


## Standard includes
use strict;
use warnings;
use File::Basename;


## Gmp includes
use Gmp::Snmp;
use Gmp::Common;


## Define script options
my %options         = ();
$options{'version'} = 'master';
$options{'name'}    = basename("$0");
$options{'display'} = 'Load average';
$options{'details'} = 'Check load average of a linux server';
# Set to 1 for debug mode.
# Do not use with Nagios, only for debugging purpose on command line.
$options{'debug'}   = 0;


# Needed oids for this plugin
my %oids        = ();
$oids{'load1'}  = '.1.3.6.1.4.1.2021.10.1.3.1';
$oids{'load5'}  = '.1.3.6.1.4.1.2021.10.1.3.2';
$oids{'load15'} = '.1.3.6.1.4.1.2021.10.1.3.3';


# Init state
my $state   = 'UNKNOWN';
my %results = ();


# Params management
my %parameters  = ();
%parameters     = build_standard_snmp_parameters(\%options);
# Local parameters
%parameters     = add_parameter(
    \%options,
    \%parameters,
    'load',
    '',
    'l',
    'load',
    '[load1|load5|load15]',
    'check only provided load 1 and/or 5 and/or 15',
    's'
);
%parameters     = get_parameters_value(\%options, \%parameters, @ARGV);


# Check general parameters
if(check_parameter(\%options, \%parameters, 'version')) {

    print_version(\%options);
    exit($ERRORS{"$state"});
}

if(check_parameter(\%options, \%parameters, 'help')) {

    print_help(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(check_snmp_parameters(\%options, \%parameters)) {

    print "\nMistake in SNMP parameters: check community for v1/v2c and sec parameters for v3\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}

if(not check_parameter(\%options, \%parameters, 'host')) {

    print "\nMissing hostname: -H|--host\n\n";
    print_usage(\%options, \%parameters);
    exit($ERRORS{"$state"});
}


# Special job for different thresholds for load1/5/15
my @loads       = ();
my %warnings    = ();
my %criticals   = ();
if(check_parameter(\%options, \%parameters, 'load')) {

    @loads=split(/,/, $parameters{'load'}{'value'});

    foreach my $load (@loads) {

        if($load !~ /^(load1|load5|load15)$/) {

            print "\nMistake in load name: must be load1 and/or load5 and/or load15\n\n";
            print_usage(\%options, \%parameters);
            exit($ERRORS{"$state"});
        }
    }
}
else {

    @loads = ( 'load1', 'load5', 'load15' );
}

if(defined($parameters{'warning'}{'value'}) and $parameters{'warning'}{'value'} =~ /,/) {

    write_debug_if_requested(\%options, '1/5/15 different thresholds on warning');

    my @warns = split(/,/, $parameters{'warning'}{'value'});

    if(scalar(@warns) != scalar(@loads)) {
        print "\nWarning threshold(s) must be <value> or <value1>,<value5>,<value15> according to --load if used\n\n";
        print_usage(\%options, \%parameters);
        exit($ERRORS{"$state"});
    }

    foreach my $load (@loads) {

        $warnings{"$load"} = shift(@warns);
    }
}
else {

    write_debug_if_requested(\%options, '1/5/15 same thresholds on warning');

    foreach my $load (@loads) {

        $warnings{"$load"} = $parameters{'warning'}{'value'};
    }
}

if(defined($parameters{'critical'}{'value'}) and $parameters{'critical'}{'value'} =~ /,/) {

    write_debug_if_requested(\%options, '1/5/15 different thresholds on critical');

    my @crits = split(/,/, $parameters{'critical'}{'value'});

    if(scalar(@crits) != scalar(@loads)) {
        print "\nCritical threshold(s) must be <value> or <value1>,<value5>,<value15> according to --load if used\n\n";
        print_usage(\%options, \%parameters);
        exit($ERRORS{"$state"});
    }

    foreach my $load (@loads) {

        $criticals{"$load"} = shift(@crits);
    }
}
else {

    write_debug_if_requested(\%options, '1/5/15 same thresholds on critical');

    foreach my $load (@loads) {

        $criticals{"$load"} = $parameters{'critical'}{'value'};
    }
}

foreach my $load (@loads) {

    if(defined($criticals{"$load"}) and defined($warnings{"$load"})
        and $criticals{"$load"} < $warnings{"$load"}) {

        print "\nCritical $load threshold must be higher then warning $load threshold\n\n";
        print_usage(\%options, \%parameters);
        exit($ERRORS{"$state"});
    }
}


# Work on snmp
SNMP:
foreach my $key (@loads) {

    write_debug_if_requested(\%options, "Snmp get key: $key");
    write_debug_if_requested(\%options, 'with OID: '.$oids{"$key"});

    $results{"$key"} = get_oid(\%options, \%parameters, $oids{"$key"});

    if(not (test_snmp_value(\%options, $results{"$key"}))) {

        write_plugin_output(\%options, $state, "Can not get $key\n");

        exit($ERRORS{"$state"});
    }

    write_debug_if_requested(\%options, "result, $key: ".$results{"$key"});
}


# Different cases of exit
my @withError = ();

THRESHOLDS:
foreach my $load (@loads) {
    if(defined($criticals{"$load"}) and $results{"$load"} > $criticals{"$load"}) {

        $state = 'CRITICAL';
        push(@withError, "$load");
    }
    elsif(defined($warnings{"$load"}) and $results{"$load"} > $warnings{"$load"} and $state ne 'CRITICAL') {

        $state = 'WARNING';
        push(@withError, "$load");
    }
}

if(scalar(@withError) == '0') {

    $state = "OK";
}


# exit of plugin
my $msg = '';
if($state eq "WARNING" or $state eq "CRITICAL") {

    ERRORS:
    foreach my $error (@withError) {

        $msg .= "$error: ".$results{"$error"}.' ';
    }
}
else {

    DISPLAY:
    foreach my $load (@loads) {

        $msg .= "[$load: ".$results{"$load"}."] ";
    }
}

write_plugin_output(\%options, $state, "$msg");

if($parameters{'perfdata'}{'value'}) {

    my %perfData = ();

    PERFDATA:
    foreach my $load (@loads) {

        $perfData{"$load"}{'value'} = $results{"$load"};
        if(defined($warnings{"$load"})) {

            $perfData{"$load"}{'warning'} = $warnings{"$load"};
        }
        if(defined($criticals{"$load"})) {

            $perfData{"$load"}{'critical'} = $criticals{"$load"};
        }
    }

    write_plugin_perfdata(\%options, \%perfData);
}
print "\n";
exit($ERRORS{"$state"});

